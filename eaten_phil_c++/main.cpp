#include <iostream>
#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <iterator>
#include <algorithm>
#include <vector>
#include <sstream>


class OSThreadSafe: public std::ostringstream
{
public:
    OSThreadSafe() = default;

    ~OSThreadSafe()
    {
        std::lock_guard<std::mutex> guard(mut);
        std::cout << this->str();
    }

private:
    static std::mutex mut;
};

std::mutex OSThreadSafe::mut{};

class Threads_Guard
{
public:
	explicit Threads_Guard(std::vector<std::thread> &thread_instance_): thread_instance(thread_instance_)
	{

	}

	~Threads_Guard()
	{
		for (auto &thread : thread_instance) {
			// auto id = thread.get_id();
			if (thread.joinable()) thread.join();
			// OSThreadSafe{} << "thread with id is " << id << " stopped!" << std::endl;
		}
		OSThreadSafe{} << "All threads are finished!" << std::endl;
	}

	Threads_Guard(const Threads_Guard & ) = delete;
	Threads_Guard & operator= (const Threads_Guard &) = delete;

private:
	std::vector<std::thread> &thread_instance;
};
        
class WaiterSem
{
public:
    explicit WaiterSem(size_t cnt_ = 0) : cnt(cnt_) 
    {

    }
    
    void notify(std::thread::id id)
	{
        std::lock_guard<std::mutex> lock(mutex);
        cnt++;
        OSThreadSafe{} << "thread " << id <<  " notify" << std::endl;
        cond_var.notify_one();
    }

	void notify(void)
	{
        std::lock_guard<std::mutex> lock(mutex);
        cnt++;
        cond_var.notify_one();
    }

    void wait(std::thread::id id)
	{
        std::unique_lock<std::mutex> lock(mutex);
            cond_var.wait(lock, [this, id]()
				{ 
					OSThreadSafe{} << "thread " << id << " wait" << std::endl;
					return cnt != 0;
				}
			);
            OSThreadSafe{} << "thread " << id << " run" << std::endl;
        cnt--;
    }

	void wait(void)
	{
        std::unique_lock<std::mutex> lock(mutex);
            cond_var.wait(lock, [this]()
				{ 
					return cnt != 0;
				}
			);
        cnt--;
    }

private:
    std::mutex mutex;
    std::condition_variable cond_var;
    int cnt;
};

int main(int argc, char* argv[])
{
	constexpr size_t PHILS_CNT = 5;

	WaiterSem sem_cnt(PHILS_CNT - 1);
	std::vector<std::mutex> forks_mut_vec(PHILS_CNT);
	std::vector<std::thread> phils_thread_vec(PHILS_CNT);

	Threads_Guard threads_guard(phils_thread_vec);

	OSThreadSafe{} << "\nThe program is running!\n" << std::flush;

	auto thread_func = [&forks_mut_vec, &sem_cnt](size_t phil_n)
	{
		sem_cnt.wait();
		forks_mut_vec[phil_n].lock();
		forks_mut_vec[(phil_n + 1) % PHILS_CNT].lock();

		OSThreadSafe{} << "Philosopher is " << phil_n << " eating" << std::endl;
		std::this_thread::sleep_for(std::chrono::seconds(1));
		OSThreadSafe{} << "Philosopher is " << phil_n << " finished eating" << std::endl;

		forks_mut_vec[(phil_n + 1) % PHILS_CNT].unlock();
		forks_mut_vec[phil_n].unlock();
		sem_cnt.notify();
	};

	for (size_t i =  0; i < PHILS_CNT; i++) {
		phils_thread_vec.emplace_back(std::thread(std::ref(thread_func), i));
	}

// std::thread thread1([&sem_cnt]() mutable
// 	{
// 		for (size_t i = 0; i <= 5; i++) {
// 		sem_cnt.wait(std::this_thread::get_id());
// 		std::cout << "thread1 run!" << std::endl;
// 		std::this_thread::sleep_for(std::chrono::seconds(1));
// 		sem_cnt.notify(std::this_thread::get_id());
// 		std::this_thread::sleep_for(std::chrono::seconds(1));
// 		}
// 	}
// );
// Threads_Guard thread1_guard(thread1);

// std::thread thread2([&sem_cnt]() mutable
// 	{
// 		for (size_t i = 0; i <= 5; i++) {
// 		sem_cnt.wait(std::this_thread::get_id());
// 		std::cout << "thread2 run!" << std::endl;
// 		std::this_thread::sleep_for(std::chrono::milliseconds(100));
// 		sem_cnt.notify(std::this_thread::get_id());
// 		}
// 	}
// );
// Threads_Guard thread2_guard(thread2);

	return 0;
}
