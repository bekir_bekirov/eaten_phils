#include <thread> 
#include <mutex> 
#include <iostream> 
#include <vector>
#include <chrono>
#include <atomic>
#include <condition_variable>
#include <functional>
#include <iterator>
#include <algorithm>
#include <sstream>

#include "dining_philosophers.h"
//const int Philosophers_No = 5;


class osThreadSafe: public std::ostringstream
{
public:
   osThreadSafe() = default;

   ~osThreadSafe()
   {
      std::lock_guard<std::mutex> guard(mut);
      std::cout << this->str();
   }

private:
   static std::mutex mut;
};

std::mutex osThreadSafe::mut{};

class Threads_Guard
{
public:
   explicit Threads_Guard(std::vector<std::thread> &thread_instance_): thread_instance(thread_instance_)
   {

   }

   ~Threads_Guard()
   {
      for (auto &thread : thread_instance) {
         // auto id = thread.get_id();
         if (thread.joinable()) thread.join();
         // osThreadSafe{} << "thread with id is " << id << " stopped!" << std::endl;
      }
      osThreadSafe{} << "All threads are finished!" << std::endl;
   }

   Threads_Guard(const Threads_Guard &) = delete;
   Threads_Guard & operator= (const Threads_Guard &) = delete;

private:
   std::vector<std::thread> &thread_instance;
};
         
class WaiterSem
{
public:
   explicit WaiterSem(size_t cnt_ = 0) : cnt(cnt_) 
   {

   }
   
   void notify(std::thread::id id)
   {
      std::lock_guard<std::mutex> lock(mutex);
      cnt++;
      osThreadSafe{} << "thread " << id <<  " notify" << std::endl;
      cond_var.notify_one();
   }

   void notify(void)
   {
      std::lock_guard<std::mutex> lock(mutex);
      cnt++;
      cond_var.notify_one();
   }

   void wait(std::thread::id id)
   {
      std::unique_lock<std::mutex> lock(mutex);
         cond_var.wait(lock, [this, id]()
         { 
            osThreadSafe{} << "thread " << id << " wait" << std::endl;
            return cnt != 0;
         }
      );
         osThreadSafe{} << "thread " << id << " run" << std::endl;
      cnt--;
   }

   void wait(void)
   {
      std::unique_lock<std::mutex> lock(mutex);
         cond_var.wait(lock, [this]()
         { 
            return cnt != 0;
         }
      );
      cnt--;
   }

private:
   std::mutex mutex;
   std::condition_variable cond_var;
   int cnt;
};

constexpr int left(int idx ) { return (idx + (Philosophers_No - 1)) % Philosophers_No; }
constexpr int right(int idx) { return (idx + 1) % Philosophers_No; }

WaiterSem waiter_sem(Philosophers_No - 1);
std::vector<std::mutex> forks_mut_vec(Philosophers_No);
std::vector<std::thread> phils_thread_vec(Philosophers_No);

std::atomic<bool> done;

dining_stat summary[Philosophers_No];

void philosopher(int idx) { 
   while (true && !done) { 
      std::this_thread::sleep_for (std::chrono::milliseconds(1)); 

      waiter_sem.wait();
      osThreadSafe{} << "Philosopher "<< idx+1<<" is Hungry"<< std::endl; 

      // take up chopsticks
      forks_mut_vec[left(idx)].lock();
      forks_mut_vec[right(idx)].lock();

      osThreadSafe{} << "Philosopher "<< idx+1<<" is Eating"<< std::endl; 
      std::this_thread::sleep_for (std::chrono::milliseconds(10)); 
      // TODO: uncomment summary update as soon as your code ready for review
      summary[idx].eat_no++;

      // put chopsticks back
      forks_mut_vec[right(idx)].unlock();
      forks_mut_vec[left(idx)].unlock();
      waiter_sem.notify();

      osThreadSafe{} << "Philosopher " << idx+1 << " is Thinking" << std::endl;
      // TODO: uncomment summary update as soon as your code ready for review
      summary[idx].think_no++; 
   }
}

void start_dining() {
   //std::cout.sync_with_stdio(true);
   std::vector<std::thread> pool;
   Threads_Guard threads_pool(pool);

   done = false;
   for (int i = 0; i < Philosophers_No; i++) { 
      pool.emplace_back(philosopher, i);
   } 
}
